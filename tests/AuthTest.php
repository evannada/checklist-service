<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserLogin()
    {
        $response = $this->call('POST', '/api/v1/login',[
            'username' => 'evannada',
            'password' => 'evannada'
        ]);
        
        $this->assertEquals(200, $response->status());
    }
}
