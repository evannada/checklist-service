<?php

use App\Http\Middleware\Authenticate;
use App\Models\Checklist;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ItemTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateChecklistItem()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->firstOrCreate([
            'description' => 'my checklist',
            'due_interval' => 3,
            'due_unit' => 'hour'
        ]);
        
        $response = $this->call('POST', '/api/v1/checklists/'.$checklist->id.'/items',[
            'token' => $token,
            'description' => 'my foo item',
            'urgency' => 2,
            'due_interval' => 40,
            'due_unit' => 'minute'
        ]);
        
        $this->assertEquals(201, $response->status());
    }

    public function testGetAllItems()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();

        $response = $this->call('GET', '/api/v1/checklists/'.$checklist->id.'/items',[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testGetItem()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();
        $item = $checklist->items()->first();

        $response = $this->call('GET', '/api/v1/checklists/'.$checklist->id.'/items/'.$item->id,[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testUpdateItem()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();
        $item = $checklist->items()->first();

        $response = $this->call('GET', '/api/v1/checklists/'.$checklist->id.'/items/'.$item->id,[
            'token' => $token,
            'description' => 'my foo item',
            'urgency' => 2,
            'due_interval' => 40,
            'due_unit' => 'minute'
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testDeleteItem()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();
        $item = $checklist->items()->first();

        $response = $this->call('DELETE', '/api/v1/checklists/'.$checklist->id.'/items/'.$item->id,[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }
}
