<?php

use App\Http\Middleware\Authenticate;
use App\Models\Checklist;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ChecklistTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateChecklist()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);
        
        $response = $this->call('POST', '/api/v1/checklists',[
            'token' => $token,
            'object_domain' => 'contact',
            'object_id' => '1',
            'description' => 'Need to verify this guy house.',
            'is_completed' => false,
            'completed_at' => NULL,
        ]);
        
        $this->assertEquals(201, $response->status());
    }

    public function testGetAllChecklists()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);
        
        $response = $this->call('GET', '/api/v1/checklists',[
            'token' => $token,
            'limit' => 5
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testGetChecklist()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();
        
        $response = $this->call('GET', '/api/v1/checklists/'.$checklist->id,[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testUpdateChecklist()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();
        
        $response = $this->call('PATCH', '/api/v1/checklists/'.$checklist->id,[
            'token' => $token,
            'object_domain' => 'contact',
            'object_id' => '1',
            'description' => 'Need to verify this guy house.',
            'is_completed' => false,
            'completed_at' => NULL,
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testDestroyChecklist()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $checklist = Checklist::take(1)->first();
        
        $response = $this->call('DELETE', '/api/v1/checklists/'.$checklist->id,[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }
}
