<?php

use App\Http\Middleware\Authenticate;
use App\Models\Template;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TemplateTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateTemplateChecklist()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);
        
        $response = $this->call('POST', '/api/v1/checklists/templates',[
            'token' => $token,
            'name' => 'foo template',
            'checklist' => [
                'description' => 'my checklist',
                'due_interval' => 3,
                'due_unit' => 'hour'
            ],
            'items' => [
                [
                'description' => 'my foo item',
                'urgency' => 2,
                'due_interval' => 40,
                'due_unit' => 'minute',
                ],
                [
                'description' => 'my bar item',
                'urgency' => 3,
                'due_interval' => 30,
                'due_unit' => 'minute',
                
                ],
            ]
        ]);
        
        $this->assertEquals(201, $response->status());
    }

    public function testGetAllChecklistsTemplates()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);
        
        $response = $this->call('GET', '/api/v1/checklists/templates',[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testGetChecklistTemplate()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $template = Template::take(1)->first();
        
        $response = $this->call('GET', '/api/v1/checklists/templates/'.$template->id,[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testUpdateChecklistTemplate()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $template = Template::take(1)->first();
        
        $response = $this->call('PATCH', '/api/v1/checklists/templates/'.$template->id,[
            'token' => $token,
            'name' => 'foo template',
            'checklist' => [
                'description' => 'my checklist',
                'due_interval' => 3,
                'due_unit' => 'hour'
            ],
            'items' => [
                [
                'description' => 'my foo item',
                'urgency' => 2,
                'due_interval' => 40,
                'due_unit' => 'minute',
                ],
                [
                'description' => 'my bar item',
                'urgency' => 3,
                'due_interval' => 30,
                'due_unit' => 'minute',
                
                ],
            ]
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    public function testDestroyChecklistTemplate()
    {
        $user = User::take(1)->first();
        $token = app('auth')->fromUser($user);

        $template = Template::take(1)->first();
        
        $response = $this->call('DELETE', '/api/v1/checklists/templates/'.$template->id,[
            'token' => $token,
        ]);
        
        $this->assertEquals(200, $response->status());
    }
}
