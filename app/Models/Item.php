<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'checklist_id', 'description', 'urgency', 'due_interval', 'due_unit',
    ];

    public function checklist()
    {
      return $this->belongsTo(Checklist::class);
    }
}