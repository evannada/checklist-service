<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function checklist()
    {
      return $this->hasOne(Checklist::class);
    }

    public function items()
    {
      return $this->hasManyThrough('App\Models\Item', 'App\Models\Checklist');
    }

}