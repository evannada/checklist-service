<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id', 'description', 'due_interval', 'due_unit',
    ];

    public function template()
    {
      return $this->belongsTo(Template::class);
    }

    public function items()
    {
      return $this->hasMany(Item::class);
    }
}