<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Checklist;
use App\Models\Template;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        $query = Template::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $templates = $query->get();
        $templates->load(['checklist', 'items']);

        $response = [
            'status' => 'success',
            'data' => $templates
        ];
        return response()->json($response, 200);
    
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'present|nullable|string',
            'checklist.description' => 'required|string',
            'checklist.due_interval' => 'present|nullable|integer',
            'checklist.due_unit' => 'present|nullable|string|in:hour,minute,day,week,month',
            'items.*.description' => 'required',
            'items.*.urgency' => 'present|nullable|integer',
            'items.*.due_interval' => 'present|nullable|integer',
            'items.*.due_unit' => 'present|nullable|string|in:hour,minute,day,week,month',
        ]);

        $template = Template::create([
            'name' => $request->name,
        ]);
        $checklist = Checklist::create([
            'template_id' => $template->id,
            'description' => $request->checklist['description'],
            'due_interval' => $request->checklist['due_interval'],
            'due_unit' => $request->checklist['due_unit'],
        ]);
        $checklist->items()->createMany($request->items);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $template->load(['checklist', 'items'])
        ];
        return response()->json($response, 201);

    }

    public function show($id)
    {
        $template = Template::with(['checklist', 'items'])->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $template
        ];
        return response()->json($response, 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'present|nullable|string',
            'checklist.description' => 'required|string',
            'checklist.due_interval' => 'present|nullable|integer',
            'checklist.due_unit' => 'present|nullable|string|in:hour,minute,day,week,month',
            'items.*.description' => 'required',
            'items.*.urgency' => 'present|nullable|integer',
            'items.*.due_interval' => 'present|nullable|integer',
            'items.*.due_unit' => 'present|nullable|string|in:hour,minute,day,week,month',
        ]);

        $template = Template::findOrFail($id);

        $template->update([
            'name' => $request->name,
        ]);

        $template->checklist()->delete();

        $checklist = Checklist::create([
            'template_id' => $template->id,
            'description' => $request->checklist['description'],
            'due_interval' => $request->checklist['due_interval'],
            'due_unit' => $request->checklist['due_unit'],
        ]);
        $checklist->items()->createMany($request->items);

        $response = [
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data' => $template->load(['checklist', 'items'])
        ];
        return response()->json($response, 200);
    }
    
    public function destroy($id)
    {
        $template = Template::findOrFail($id);
        $template->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}