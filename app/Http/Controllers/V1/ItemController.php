<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Checklist;

class ItemController extends Controller
{
    public function index(Request $request, $checklist_id)
    {
        $this->validate($request, [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        $query = Checklist::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $checklistItems = $query->findOrFail($checklist_id);
        $checklistItems->load('items');

        $response = [
            'status' => 'success',
            'data' => $checklistItems
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request, $checklist_id)
    {
        $this->validate($request, [
            'description' => 'required',
            'urgency' => 'present|nullable|integer',
            'due_interval' => 'present|nullable|integer',
            'due_unit' => 'present|nullable|string|in:hour,minute,day,week,month',
        ]);

        $checklist = Checklist::findOrFail($checklist_id);
        $item = $checklist->items()->create($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $item
        ];
        return response()->json($response, 201);

    }

    public function show($checklist_id, $item_id)
    {
        $checklistItem = Checklist::findOrFail($checklist_id)->items()->findOrFail($item_id);

        $response = [
            'status' => 'success',
            'data' => $checklistItem
        ];
        return response()->json($response, 200);
    }

    public function update(Request $request, $checklist_id, $item_id)
    {
        $checklistItem = Checklist::findOrFail($checklist_id)->items()->findOrFail($item_id);
        $checklistItem->update($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data' => $checklistItem
        ];
        return response()->json($response, 200);
    }

    public function destroy($checklist_id, $item_id)
    {
        $checklistItem = Checklist::findOrFail($checklist_id)->items()->findOrFail($item_id);
        $checklistItem->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record delete successfully.',
        ];
        return response()->json($response, 200);
    }
}