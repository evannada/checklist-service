<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Checklist;
use Illuminate\Support\Facades\Validator;

class ChecklistController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Checklist::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $checklists = $query->get();

        $response = [
            'status' => 'success',
            'data' => $checklists
        ];
        return response()->json($response, 200);
    
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'object_domain' => 'required|string',
            'object_id' => 'required|string',
            'description' => 'required|string',
            'is_completed' => 'nullable|boolean',
            'completed_at' => 'nullable|date_format:Y-m-d H:i:s',
        ]);
        
        $checklist = Checklist::create($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $checklist
        ];
        return response()->json($response, 201);

    }

    public function show($id)
    {
        $checklist = Checklist::findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $checklist
        ];
        return response()->json($response, 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'object_domain' => 'required|string',
            'object_id' => 'required|string',
            'description' => 'required|string',
            'is_completed' => 'nullable|boolean',
            'completed_at' => 'nullable|date_format:Y-m-d H:i:s',
        ]);

        $checklist = Checklist::findOrFail($id);
        $checklist->update($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data' => $checklist
        ];
        return response()->json($response, 200);
    }
    
    public function destroy($id)
    {
        $checklist = Checklist::findOrFail($id);
        $checklist->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}