<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Checklist;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $token = app('auth')->attempt($request->only('username', 'password'));
        return response()->json(compact('token'));
    }
}