<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api/v1'], function () use ($router) {
  
    $router->post('login', 'V1\AuthController@login');

    //For authenticate user
    $router->group(['middleware' => 'auth'], function () use ($router) {
        
        //Template
        $router->get('checklists/templates', 'V1\TemplateController@index');
        $router->post('checklists/templates', 'V1\TemplateController@store');
        $router->get('checklists/templates/{id}', 'V1\TemplateController@show');
        $router->patch('checklists/templates/{id}', 'V1\TemplateController@update');
        $router->delete('checklists/templates/{id}', 'V1\TemplateController@destroy');

        //Checklist
        $router->get('checklists', 'V1\ChecklistController@index');
        $router->post('checklists', 'V1\ChecklistController@store');
        $router->get('checklists/{id}', 'V1\ChecklistController@show');
        $router->patch('checklists/{id}', 'V1\ChecklistController@update');
        $router->delete('checklists/{id}', 'V1\ChecklistController@destroy');

        //Item
        $router->get('checklists/{checklist_id}/items', 'V1\ItemController@index');
        $router->post('checklists/{checklist_id}/items', 'V1\ItemController@store');
        $router->get('checklists/{checklist_id}/items/{item_id}', 'V1\ItemController@show');
        $router->patch('checklists/{checklist_id}/items/{item_id}', 'V1\ItemController@update');
        $router->delete('checklists/{checklist_id}/items/{item_id}', 'V1\ItemController@destroy');

    });
});
